from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _


class Author(models.Model):
    name = models.CharField(_('Author'), max_length=254)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Post(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    title = models.CharField(_('Title'), max_length=254)
    content = models.TextField(_('Content'))
    published_date = models.DateField(_('Published date'))
    link = models.CharField(_('Link'), max_length=254)

    class Meta:
        ordering = ('-published_date',)

    def __str__(self):
        return self.title
