from django.shortcuts import render
from django.views import generic

from . import models


class PostListView(generic.ListView):
    model = models.Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['authors'] = models.Author.objects.all()
        return context
