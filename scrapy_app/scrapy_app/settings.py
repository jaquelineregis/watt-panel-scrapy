import os
import sys
import django


sys.path.append(os.path.dirname(os.path.abspath('.')))
os.environ['DJANGO_SETTINGS_MODULE'] = 'main.settings'

django.setup()

BOT_NAME = 'scrapy_app'

SPIDER_MODULES = ['scrapy_app.spiders']
NEWSPIDER_MODULE = 'scrapy_app.spiders'

ITEM_PIPELINES = {
    'scrapy_app.pipelines.ScrapyAppPipeline': 300,
}
