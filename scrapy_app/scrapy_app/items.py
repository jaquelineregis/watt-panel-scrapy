import scrapy
from scrapy_djangoitem import DjangoItem

from blog import models


class PostItem(DjangoItem):
    django_model = models.Post
