from scrapy.selector import Selector
from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from datetime import datetime

from .. import items


class WattPanelSpider(CrawlSpider):
    name = 'blog_spider'
    start_urls = [
        'http://blog.wattpanel.com.br',
        'http://blog.wattpanel.com.br/page/2/',
    ]

    def parse(self, response):
        selector = Selector(response)
        urls = selector.xpath('//a[@class="more-link"]/@href').extract()

        for url in urls:
            yield Request(url, self.parse_post)

    def parse_post(self, response):
        selector = Selector(response)

        author = selector.xpath('//div[@class="entry-meta"]//span[2]//a//text()').extract_first()
        published_date = self.to_date(selector.xpath('//div[@class="entry-meta"]//span[1]//a//time[1]//text()'))
        title = selector.xpath('//h1[@class="entry-title"]//text()').extract_first()
        content = selector.xpath('//div[@class="entry-content"]//text()').extract()
        link = selector.xpath('//div[@class="entry-meta"]//span[1]//a/@href').extract_first()

        return items.PostItem(
            author=author,
            title=title,
            content=content,
            published_date=published_date,
            link=link
            )

    def to_date(self, element):
        day, month, year = [int(x) for x in element.extract_first().split("/")]
        return datetime(year, month, day)
