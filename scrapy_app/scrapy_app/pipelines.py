from blog import models


class ScrapyAppPipeline(object):
    def process_item(self, item, spider):
        post_exists = models.Post.objects.filter(
                        author__name=item['author'],
                        title=item['title'],
                        published_date=item['published_date']
                        ).exists()

        if not post_exists:
            author, _ = models.Author.objects.get_or_create(name=item['author'])
            item['author'] = author
            item.save()

        return item

