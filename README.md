# Crawling data in Watt Panel Blog

Django | Scrapy | Bootstrap

##### Steps:

1. terminal:
```
pip install -r requirements.txt

python manage.py migrate

python manage.py createsuperuser

python manage.py runserver
```
2. Access:
[localhost:8000](http://127.0.0.1:8000)

3. another terminal:
```
cd scrapy_app
scrapy crawl blog_spider
```